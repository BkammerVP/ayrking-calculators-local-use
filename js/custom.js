$=jQuery;
$(function() {                
	
	//mobile nav trigger
	$('.mobile-trigger .btn').on('click', function(){
		$('.nav-buttons.mobile').slideToggle('fast');
	})
	
	//handle search terms checkbox hierarchy
	$('.first-tier > .item > .input-contain > input.hierarchy').on('change', function(){
		showOnChecked($(this), $(this).parent().parent().find('.second-tier'));
	});   
	$('.second-tier > .item > .input-contain-second > input').on('change', function(){
	 	showOnChecked($(this), $(this).parent().parent().find('.third-tier'));
	});
	
	$('.second-tier > .item > .input-contain-second > input').on('change', function(){
	  	$(this).parents('.first-tier').find('.input-contain input').attr('checked', false);
	});
	
	$('.third-tier > .item > .input-contain-third > input').on('change', function(){
		$(this).parents('.second-tier').find('.input-contain-second input').attr('checked', false);
		$(this).parents('.first-tier').find('.input-contain input').attr('checked', false);
	});   
	
	//select ALL for resource/document
	$('.term-all-resource-types input, .term-all-document-types input').on('change', function(){
		var cb = $(this);
		var all_inputs = $(this).closest('.first-tier').find('input');
		if ($(cb).attr('checked') == 'checked') {
			$(all_inputs).attr('checked', 'checked');
		} else {
			$(all_inputs).removeAttr('checked');
			
		}
	});
	//resource uncheck
	$('#by_resource_type input').not('.term-all-resource-types input').on('change', function(){
		var cb = $(this);
		if ($(cb).attr('checked') != 'checked') {
			$('.term-all-resource-types input').removeAttr('checked');
		} else {

		}
	}) 
	//document uncheck
	$('#by_document_type input').not('.term-all-document-types input').on('change', function(){
		var cb = $(this);
		if ($(cb).attr('checked') != 'checked') {
			$('.term-all-document-types input').removeAttr('checked');
		} else {

		}
	})
	

//show submenus
$('.menu-item-has-children > a').click(function(e) { 
	var submenu = $(this).parent().find('.sub-menu');  
	if ($(submenu).is(":visible") && $(this).html() != "Products" && $(this).html() != "Markets") { 
		
	 } else {      
		
		e.preventDefault();  

	  	$('.sub-menu').not(submenu).slideUp('fast');
		  
		$('.menu-item-has-children').removeClass('current-nav-item');		
		
		$(this).parent().addClass('current-nav-item');		
		$(submenu).slideDown('fast');      
		
	}   
})     

// top nav arrow
$('.menu-item-has-children').each(function(){
	var arrow_html = "<div class='current-arrow'></div>";
	$(this).append(arrow_html);
})

//show history blocks 
$('.year').click(function(){ 
	var index = $(this).index();
	var current_content = $(this).parent().parent().find('.year-content').eq(index);
	$('.year').not($(this)).removeClass('current');
	$(this).toggleClass('current');
	
	$('.year-content').not(current_content).slideUp('fast');
	$(current_content).slideToggle('fast');
})

//show spot text
$('.spot').click(function(){
	$('.spot').not($(this)).removeClass('current');
	$(this).addClass('current');
	                                   
	$('.text').not($(this).find('.text')).slideUp('fast');
	$(this).find('.text').slideToggle('fast');
})

//show bios
$('.bio-link').click(function(){
	var bio_html = $(this).find('.bio-full').html();
	var position = $(this).position(); 
	var img_height = $(this).find('img').height();
	
	$('.bio-link').removeClass('current');
	$(this).addClass('current');
	
	$('.bio-load .content').html(bio_html);
	$('.bio-load').css('top', position.top + img_height -1);
	$('.bio-load').fadeIn('fast');
	animateScroll($('.bio-load'), 400, 0);
})  

//show testimonials
$('.testimonial-link').click(function(){
	var testimonial_html = $(this).find('.testimonial-full').html();
	var position = $(this).position(); 
	var img_height = $(this).find('img').height();
	var load_container = $(this).next('.testimonial-load');
	
	$(load_container).find('.content').html(testimonial_html);
	$(load_container).css('top', position.top + img_height);
	$('.testimonial-load').fadeOut('fast');
	$(load_container).fadeIn('fast');
	animateScroll($(load_container), 400, 0);
})

//sales-service handler
$('#input-country').on('change', function(){
	var country = $(this).val();
	
	if (country == "united-states") {
		$('.states').removeClass('hide');
	} else {  
		$('#input-state').val('all');
		$('.states').addClass('hide');
	}
	
}) 

//sales-submit handler
$('.sales-submit').on('click', function(){
	var region = $("#input-country").find(':selected').data('region');
	var country = $("#input-country").val();
   	var state = $("#input-state").val();

	$('.map-spot').removeClass('reveal');
	$('.map-spot.' + region).addClass('reveal'); 
	
	loadDistributors(country, state);
})

//close modal
$('.closer').click(function(){
	$(this).parent().fadeOut('fast'); 
	$('.content-contain *').removeClass('current'); 
	$('html').removeClass('static');
	
})

//cycle
if(jQuery().cycle) {
               
	if ($('.gallery .slide').length >= 1) {    
		
		$( '.gallery' ).on( 'cycle-post-initialize', function() {

			setTimeout(function(){ 

				var gallery_pos = ($('.gallery').height() / 2) - ($('.prev').height() / 2);
				$('.gallery-nav-arrows').css('top', gallery_pos );
				$('.gallery-nav-arrows').addClass('reveal');

				}, 500);

			}); 
         
		  $('.gallery').cycle({
				speed: 400,
				timeout: 0,
				swipe: "true",
				fx: "scrollHorz",
				slides: "> div",
				prev: ".gallery-contain .prev",
				next: ".gallery-contain .next"

			});

	        if ($('.gallery .slide').length > 3) {
	          	$('.carousel-nav').cycle({
					speed: 400,
					timeout: 0,
					swipe: "true",
					fx: "carousel",
					carouselVisible:"3",
					slides: "> .tile",
					prev: ".carousel-nav-contain .prev",
					next: ".carousel-nav-contain .next"

				});
			} else {
			  $('.carousel-nav-contain').addClass('static');  
			}  
			
			if ($('.gallery .slide').length <= 1) { 
				$('.gallery-nav-arrows').addClass('static');  
			}

}  

if ($('#home-banner .slide').length > 1) {

	$('#home-banner').cycle({
		speed: 400,
		timeout: 8000,
		swipe: "true",
		fx: "fade",
		slides: "> div",
		prev: ".gallery-contain .prev",
		next: ".gallery-contain .next"

	});

}

 	$('.carousel-nav .tile').click(function(e){
			var index = $(this).attr('data-index');
			$('.gallery-contain').fadeIn('fast', function(){ 
				$('html').addClass('static');
				$('.gallery').cycle('goto', index);
			})
		});


} //end cycle
    
//PRODUCTS > spec sheet handlers

$('.open-ss').click(function(e){
	e.preventDefault();
	$('.ss-contain').fadeIn('fast');
});
	
//PRODUCTS > calculate handlers

$('.calculator #input-unit').on('change', function(){
	$(this).parent().find('.custom_unit').html($(this).val());
})      

$('.calculator input[type="number"]').on('change', function(){
	var input = $(this);
   	var value = $(this).val(); 

  	if (value.includes("-")) {
		$(input).val(value.replace("-", ""));
	} 
})

$('#calculate').on('click', function(e){
	e.preventDefault();
	switch($(this).attr('data-calculator')){
		case "breader-blender-sifter":
		calculateBBS();
		break;
		case "drumroll-automated-breader":
		calculateDrumRoll();
		break;
		case "labor-savings":
		case "labor-savings-calculator":
		calculateLaborSavings();
		break;
	}
	 
	$('.calc-results').slideDown('fast');
	animateScroll($(this), 400, 0);
	$(this).text('REFRESH');
});

// MARKETS	
	
$('#retail-menu a').on('click', function(e){
	e.preventDefault();
	var index = $(this).parent().index();
	$('.retail-block').slideUp('fast');
	$('.retail-block').eq(index).slideDown('fast');
})	
	
	
}); //end jQuery
    
$(document).ready(function(){ 
         

$('#gform_submit_button_1').addClass('scale-hover self');

// **EXPLODE search options based on result
                                                
$('.has-results input:checked, .item input:checked').parent().parent().addClass('highlighted');
$('.has-results .third-tier input:checked').parent().parent().parent().parent().addClass('highlighted');
$('.third-tier input:checked').closest('.first-tier > .item').addClass('highlighted');
$('.second-tier input:checked').closest('.first-tier > .item').addClass('highlighted');

// show second tier if top-level item checked
$('.first-tier > .item > .input-contain > input.hierarchy:checked').parent().parent().find('.second-tier').css('display','block'); 

// show second tier if second tier item checked
$('.second-tier > .item > .input-contain-second > input:checked').closest('.second-tier').css('display','block');
                                          
// show third tier if second tier item checked
$('.second-tier > .item > .input-contain-second > input:checked').parent().parent().find('.third-tier').css('display','block');
                                              
// show third tier if third tier item checked
$('.third-tier > .item > .input-contain-third > input:checked').closest('.third-tier').css('display','block');
   
})
        
//calculateBBS
function calculateBBS(){
	var cost = $('#input-cost').val();
	var unit = $('#input-unit').val();
	var amount = $('#input-amount').val();
	var time_inc = $('#input-time-inc').val(); 
	var single_store_cost = (cost * amount * time_inc) * 0.35;


$('.calc-store').each(function(){
	var stores = $(this).attr('data-stores'); 
	$(this).find('td:nth-child(2)').text(accounting.formatMoney(single_store_cost * stores));
	$(this).find('td:nth-child(3)').text(accounting.formatMoney((single_store_cost * stores) * 5));
	$(this).find('td:nth-child(4)').text(accounting.formatMoney((single_store_cost * stores) * 10));
})
	
}

//calculateBBS
function calculateLaborSavings(){
	var op_earnings = $('#input-op-earnings').val();
	var sift_time = $('#input-sift-time').val() / 60;
	var times_day = $('#input-times-day').val();
	
	var bbs_sift_time = 0.016666667; 
	
	var current_cost = ((op_earnings*sift_time)*times_day)*350;
	var bbs_cost = ((op_earnings*bbs_sift_time)*times_day)*350;
	
	var single_store_cost = current_cost - bbs_cost;


$('.calc-store').each(function(){
	var stores = $(this).attr('data-stores'); 
	$(this).find('td:nth-child(2)').text(accounting.formatMoney(single_store_cost * stores));
	$(this).find('td:nth-child(3)').text(accounting.formatMoney((single_store_cost * stores) * 5));
	$(this).find('td:nth-child(4)').text(accounting.formatMoney((single_store_cost * stores) * 10));
})
	
}

//calculateDrumRoll
function calculateDrumRoll(){
	var fry_cost = $('#frying #input-cost').val();
	var fry_unit = $('#frying #input-unit').val();
	var fry_amount = $('#frying #input-amount').val();
	var fry_time_inc = $('#frying #input-time-inc').val(); 
	
	var breading_cost = $('#breading #input-cost').val();
	var breading_unit = $('#breading #input-unit').val();
	var breading_amount = $('#breading #input-amount').val();
	var breading_time_inc = $('#breading #input-time-inc').val();
		
	var fry_total = (fry_cost * fry_amount * fry_time_inc) * 0.05;
	var breading_total = (breading_cost * breading_amount * breading_time_inc) * 0.43;	
	
	var single_store_cost = fry_total + breading_total;

$('.calc-store').each(function(){
	var stores = $(this).attr('data-stores'); 
	$(this).find('td:nth-child(2)').text(accounting.formatMoney(single_store_cost * stores));
	$(this).find('td:nth-child(3)').text(accounting.formatMoney((single_store_cost * stores) * 5));
	$(this).find('td:nth-child(4)').text(accounting.formatMoney((single_store_cost * stores) * 10));
})
	
}

//loadDistributor 

function loadDistributors(country, state){
	$.ajax({  
		type: 'POST',
		dataType: 'json',
		url: ajax_hook.ajax_url,
		data: ({   
			action: 'loadDistributor',
            country: country,
            state: state
			}),
			success: function(response){    
				$('.distributor-result').html('');
				response.forEach(listDistributors);
				$('.your-results').slideDown('fast');
				animateScroll($('.your-results'))
				
			}                         

		});   
	}       
	
//listDistributors
	function listDistributors(item, index){  
		 
		$('.distributor-result').append('<h3>'+item.name+'</h3><table class="dist-result" data-index='+index+'></table>');
		if (item.meta.contact != "") $('table.dist-result[data-index='+index+']').append('<tr><td>Contact:</td><td>'+item.meta.contact+'</td></tr>');
		if (item.meta.email != "") $('table.dist-result[data-index='+index+']').append('<tr><td>Email:</td><td><a href="mailto:'+item.meta.email+'">'+item.meta.email+'</a></td></tr>');
		if (item.meta.phone != "") $('table.dist-result[data-index='+index+']').append('<tr><td>Phone:</td><td>'+item.meta.phone+'</td></tr>');
		if (item.meta.fax != "") $('table.dist-result[data-index='+index+']').append('<tr><td>Fax:</td><td>'+item.meta.fax+'</td></tr>');
		if (item.meta.address != "") $('table.dist-result[data-index='+index+']').append('<tr><td>Address:</td><td>'+item.meta.address+'</td></tr>');
		if (item.meta.website != "") $('table.dist-result[data-index='+index+']').append('<tr><td>Web:</td><td><a href="'+item.meta.website+'" target="_blank">'+item.meta.website+'</a></td></tr>'); 
		if (item.meta.country != "") $('table.dist-result[data-index='+index+']').append('<tr><td>Country:</td><td>'+item.meta.country+'</td></tr>'); 

	}    


//showOnChecked
function showOnChecked(trigger, target){ 
		if ($(trigger).is(':checked')){
		  	$(target).slideDown('fast');
		} else {
		   $(target).slideUp('fast');
		}
}    

// animateScroll: scrolls to target
function animateScroll(target, time, extra_offset){
	
	if (extra_offset === undefined) {
          extra_offset = 0;
    }
	
	var offset = $(target).offset();
	$('html, body').animate({        
		scrollTop:offset.top - extra_offset
		}, time);
	}
